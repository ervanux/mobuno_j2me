/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

/**
 *
 * @author e
 */
public class Card {

    private short number = -1;
    private short color = -1;
    private short inPicture = -1;

    public Card(short number, short color, short inPicture) {
        this.number = number;
        this.color = color;
        this.inPicture = inPicture;

    }

    public int getColor() {
        return color;
    }

    public int getNumber() {
        return number;
    }

    public short getInPicture() {
        return inPicture;
    }

    public void setInPicture(short inPicture) {
        this.inPicture = inPicture;
    }

    public String toString() {
        String s = "";
        switch (color) {
            case Box.COLOR_BLUE:
                s = "Blue";
                break;
            case Box.COLOR_GREEN:
                s = "Green";
                break;
            case Box.COLOR_RED:
                s = "Red";
                break;
            case Box.COLOR_YELLOW:
                s = "Yellow";
                break;
            case Box.COLOR_WILD:
                s = "Wıld";
                break;
            default:
                s = "Other";
        }
        s += " " + number;
        return s;
    }
}


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Stack;
import javax.microedition.lcdui.game.TiledLayer;

/**
 *
 * @author e
 */
public class Down {

    TiledLayer tl;
    Stack cards;

    public Down(TiledLayer tl) {
        this.tl = tl;
        cards = new Stack();
    }

    public void takefirstCard(Card card) {
        cards.push(card);
        tl.setCell(0, 0, card.getInPicture());
    }

    public void takeCard(Card newCard) {
        if (canBe(newCard)) {
            cards.push(newCard);
            tl.setCell(0, 0, newCard.getInPicture());
        }
    }

    public Card getTopCard() {
        return (Card) cards.peek();
    }

    boolean canBe(Card newCard) {
        Card downCard = (Card) cards.peek();
        if (newCard.getColor() == Box.COLOR_WILD) {//wild card
            return true;
        } else if (downCard.getColor() == Box.COLOR_WILD) {//wild card
            return true;
        } else if (newCard.getColor() == downCard.getColor()) {//same color
            return true;
        } else if (newCard.getNumber() == downCard.getNumber()) {//same number
            return true;
        } else {
            return false;
        }
    }
}

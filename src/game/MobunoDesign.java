/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;

/**
 * @author e
 */
public class MobunoDesign {

    //<editor-fold defaultstate="collapsed" desc=" Generated Fields ">//GEN-BEGIN:|fields|0|
    private Image back;
    private TiledLayer backLayer;
    private Image gamecurs;
    private TiledLayer cursorLayer;
    private Image cards;
    private TiledLayer down1Layer;
    private Image sidecursors;
    private TiledLayer myCardLayer;
    private TiledLayer other1;
    private TiledLayer cursorRightLayer;
    private TiledLayer other2;
    private TiledLayer cursorLeftLayer;
    private TiledLayer other3;
    private TiledLayer other5;
    private TiledLayer other4;
    private TiledLayer other7;
    private TiledLayer other6;
    private TiledLayer other9;
    private TiledLayer other8;
    private TiledLayer down2Layer;
    private TiledLayer boxLayer;
    private TiledLayer other1cards;
    private Image numbers;
    private TiledLayer others9Card;
    private TiledLayer boxCard;
    private TiledLayer myCarsCount;
    private TiledLayer others6Card;
    private TiledLayer others5card;
    private TiledLayer others8Card;
    private TiledLayer others7Card;
    private TiledLayer others2Card;
    private TiledLayer others4Card;
    private TiledLayer others3Card;
    //</editor-fold>//GEN-END:|fields|0|

    //<editor-fold defaultstate="collapsed" desc=" Generated Methods ">//GEN-BEGIN:|methods|0|
    //</editor-fold>//GEN-END:|methods|0|
    public void updateLayerManagerForScene(LayerManager lm) throws java.io.IOException {//GEN-LINE:|1-updateLayerManager|0|1-preUpdate
        // write pre-update user code here
        getCursorLayer().setPosition(22, 136);//GEN-BEGIN:|1-updateLayerManager|1|1-postUpdate
        getCursorLayer().setVisible(true);
        lm.append(getCursorLayer());
        getMyCardLayer().setPosition(18, 114);
        getMyCardLayer().setVisible(true);
        lm.append(getMyCardLayer());
        getBoxCard().setPosition(90, 70);
        getBoxCard().setVisible(true);
        lm.append(getBoxCard());
        getMyCarsCount().setPosition(81, 96);
        getMyCarsCount().setVisible(true);
        lm.append(getMyCarsCount());
        getOthers9Card().setPosition(111, 93);
        getOthers9Card().setVisible(true);
        lm.append(getOthers9Card());
        getOthers8Card().setPosition(114, 65);
        getOthers8Card().setVisible(true);
        lm.append(getOthers8Card());
        getOthers7Card().setPosition(111, 37);
        getOthers7Card().setVisible(true);
        lm.append(getOthers7Card());
        getOthers6Card().setPosition(155, 11);
        getOthers6Card().setVisible(true);
        lm.append(getOthers6Card());
        getOthers5card().setPosition(99, 11);
        getOthers5card().setVisible(true);
        lm.append(getOthers5card());
        getOthers4Card().setPosition(39, 12);
        getOthers4Card().setVisible(true);
        lm.append(getOthers4Card());
        getOthers3Card().setPosition(40, 39);
        getOthers3Card().setVisible(true);
        lm.append(getOthers3Card());
        getOthers2Card().setPosition(40, 65);
        getOthers2Card().setVisible(true);
        lm.append(getOthers2Card());
        getOther1cards().setPosition(43, 93);
        getOther1cards().setVisible(true);
        lm.append(getOther1cards());
        getCursorLeftLayer().setPosition(4, 115);
        getCursorLeftLayer().setVisible(false);
        lm.append(getCursorLeftLayer());
        getCursorRightLayer().setPosition(162, 114);
        getCursorRightLayer().setVisible(false);
        lm.append(getCursorRightLayer());
        getDown2Layer().setPosition(90, 34);
        getDown2Layer().setVisible(true);
        lm.append(getDown2Layer());
        getBoxLayer().setPosition(71, 64);
        getBoxLayer().setVisible(true);
        lm.append(getBoxLayer());
        getDown1Layer().setPosition(69, 34);
        getDown1Layer().setVisible(true);
        lm.append(getDown1Layer());
        getOther9().setPosition(133, 87);
        getOther9().setVisible(true);
        lm.append(getOther9());
        getOther8().setPosition(133, 57);
        getOther8().setVisible(true);
        lm.append(getOther8());
        getOther7().setPosition(134, 28);
        getOther7().setVisible(true);
        lm.append(getOther7());
        getOther6().setPosition(121, 0);
        getOther6().setVisible(true);
        lm.append(getOther6());
        getOther5().setPosition(65, 1);
        getOther5().setVisible(true);
        lm.append(getOther5());
        getOther4().setPosition(4, 1);
        getOther4().setVisible(true);
        lm.append(getOther4());
        getOther3().setPosition(0, 28);
        getOther3().setVisible(true);
        lm.append(getOther3());
        getOther2().setPosition(1, 57);
        getOther2().setVisible(true);
        lm.append(getOther2());
        getOther1().setPosition(0, 87);
        getOther1().setVisible(true);
        lm.append(getOther1());
        getBackLayer().setPosition(0, 0);
        getBackLayer().setVisible(true);
        lm.append(getBackLayer());//GEN-END:|1-updateLayerManager|1|1-postUpdate
        // write post-update user code here
    }//GEN-BEGIN:|1-updateLayerManager|2|
//GEN-END:|1-updateLayerManager|2|
    public Image getBack() throws java.io.IOException {//GEN-BEGIN:|2-getter|0|2-preInit
        if (back == null) {//GEN-END:|2-getter|0|2-preInit
            // write pre-init user code here
            back = Image.createImage("/data/back.png");//GEN-BEGIN:|2-getter|1|2-postInit
        }//GEN-END:|2-getter|1|2-postInit
        // write post-init user code here
        return this.back;//GEN-BEGIN:|2-getter|2|
    }
//GEN-END:|2-getter|2|
    public TiledLayer getBackLayer() throws java.io.IOException {//GEN-BEGIN:|3-getter|0|3-preInit
        if (backLayer == null) {//GEN-END:|3-getter|0|3-preInit
            // write pre-init user code here
            backLayer = new TiledLayer(1, 1, getBack(), 176, 208);//GEN-BEGIN:|3-getter|1|3-midInit
            int[][] tiles = {
                { 1 }
            };//GEN-END:|3-getter|1|3-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3-getter|2|3-postInit
                for (int col = 0; col < 1; col++) {
                    backLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3-getter|2|3-postInit
        // write post-init user code here
        return backLayer;//GEN-BEGIN:|3-getter|3|
    }
//GEN-END:|3-getter|3|
    public Image getGamecurs() throws java.io.IOException {//GEN-BEGIN:|4-getter|0|4-preInit
        if (gamecurs == null) {//GEN-END:|4-getter|0|4-preInit
            // write pre-init user code here
            gamecurs = Image.createImage("/data/gamecurs.png");//GEN-BEGIN:|4-getter|1|4-postInit
        }//GEN-END:|4-getter|1|4-postInit
        // write post-init user code here
        return this.gamecurs;//GEN-BEGIN:|4-getter|2|
    }
//GEN-END:|4-getter|2|
    public TiledLayer getCursorLayer() throws java.io.IOException {//GEN-BEGIN:|7-getter|0|7-preInit
        if (cursorLayer == null) {//GEN-END:|7-getter|0|7-preInit
            // write pre-init user code here
            cursorLayer = new TiledLayer(1, 1, getGamecurs(), 10, 10);//GEN-BEGIN:|7-getter|1|7-midInit
            int[][] tiles = {
                { 1 }
            };//GEN-END:|7-getter|1|7-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|7-getter|2|7-postInit
                for (int col = 0; col < 1; col++) {
                    cursorLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|7-getter|2|7-postInit
        // write post-init user code here
        return cursorLayer;//GEN-BEGIN:|7-getter|3|
    }
//GEN-END:|7-getter|3|
    public Image getCards() throws java.io.IOException {//GEN-BEGIN:|8-getter|0|8-preInit
        if (cards == null) {//GEN-END:|8-getter|0|8-preInit
            // write pre-init user code here
            cards = Image.createImage("/data/cards.png");//GEN-BEGIN:|8-getter|1|8-postInit
        }//GEN-END:|8-getter|1|8-postInit
        // write post-init user code here
        return this.cards;//GEN-BEGIN:|8-getter|2|
    }
//GEN-END:|8-getter|2|
    public TiledLayer getDown1Layer() throws java.io.IOException {//GEN-BEGIN:|9-getter|0|9-preInit
        if (down1Layer == null) {//GEN-END:|9-getter|0|9-preInit
            // write pre-init user code here
            down1Layer = new TiledLayer(1, 1, getCards(), 20, 27);//GEN-BEGIN:|9-getter|1|9-midInit
            int[][] tiles = {
                { 1 }
            };//GEN-END:|9-getter|1|9-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|9-getter|2|9-postInit
                for (int col = 0; col < 1; col++) {
                    down1Layer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|9-getter|2|9-postInit
        // write post-init user code here
        return down1Layer;//GEN-BEGIN:|9-getter|3|
    }
//GEN-END:|9-getter|3|
    public TiledLayer getMyCardLayer() throws java.io.IOException {//GEN-BEGIN:|12-getter|0|12-preInit
        if (myCardLayer == null) {//GEN-END:|12-getter|0|12-preInit
            // write pre-init user code here
            myCardLayer = new TiledLayer(7, 1, getCards(), 20, 27);//GEN-BEGIN:|12-getter|1|12-midInit
            int[][] tiles = {
                { 1, 16, 31, 46, 5, 20, 35 }
            };//GEN-END:|12-getter|1|12-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|12-getter|2|12-postInit
                for (int col = 0; col < 7; col++) {
                    myCardLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|12-getter|2|12-postInit
        // write post-init user code here
        return myCardLayer;//GEN-BEGIN:|12-getter|3|
    }
//GEN-END:|12-getter|3|
    public TiledLayer getOther1() throws java.io.IOException {//GEN-BEGIN:|13-getter|0|13-preInit
        if (other1 == null) {//GEN-END:|13-getter|0|13-preInit
            // write pre-init user code here
            other1 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|13-getter|1|13-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|13-getter|1|13-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|13-getter|2|13-postInit
                for (int col = 0; col < 4; col++) {
                    other1.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|13-getter|2|13-postInit
        // write post-init user code here
        return other1;//GEN-BEGIN:|13-getter|3|
    }
//GEN-END:|13-getter|3|
    public TiledLayer getOther2() throws java.io.IOException {//GEN-BEGIN:|14-getter|0|14-preInit
        if (other2 == null) {//GEN-END:|14-getter|0|14-preInit
            // write pre-init user code here
            other2 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|14-getter|1|14-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|14-getter|1|14-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|14-getter|2|14-postInit
                for (int col = 0; col < 4; col++) {
                    other2.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|14-getter|2|14-postInit
        // write post-init user code here
        return other2;//GEN-BEGIN:|14-getter|3|
    }
//GEN-END:|14-getter|3|
    public TiledLayer getOther3() throws java.io.IOException {//GEN-BEGIN:|15-getter|0|15-preInit
        if (other3 == null) {//GEN-END:|15-getter|0|15-preInit
            // write pre-init user code here
            other3 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|15-getter|1|15-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|15-getter|1|15-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|15-getter|2|15-postInit
                for (int col = 0; col < 4; col++) {
                    other3.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|15-getter|2|15-postInit
        // write post-init user code here
        return other3;//GEN-BEGIN:|15-getter|3|
    }
//GEN-END:|15-getter|3|
    public TiledLayer getOther4() throws java.io.IOException {//GEN-BEGIN:|16-getter|0|16-preInit
        if (other4 == null) {//GEN-END:|16-getter|0|16-preInit
            // write pre-init user code here
            other4 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|16-getter|1|16-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|16-getter|1|16-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|16-getter|2|16-postInit
                for (int col = 0; col < 4; col++) {
                    other4.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|16-getter|2|16-postInit
        // write post-init user code here
        return other4;//GEN-BEGIN:|16-getter|3|
    }
//GEN-END:|16-getter|3|
    public TiledLayer getOther5() throws java.io.IOException {//GEN-BEGIN:|17-getter|0|17-preInit
        if (other5 == null) {//GEN-END:|17-getter|0|17-preInit
            // write pre-init user code here
            other5 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|17-getter|1|17-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|17-getter|1|17-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|17-getter|2|17-postInit
                for (int col = 0; col < 4; col++) {
                    other5.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|17-getter|2|17-postInit
        // write post-init user code here
        return other5;//GEN-BEGIN:|17-getter|3|
    }
//GEN-END:|17-getter|3|
    public TiledLayer getOther6() throws java.io.IOException {//GEN-BEGIN:|18-getter|0|18-preInit
        if (other6 == null) {//GEN-END:|18-getter|0|18-preInit
            // write pre-init user code here
            other6 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|18-getter|1|18-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|18-getter|1|18-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|18-getter|2|18-postInit
                for (int col = 0; col < 4; col++) {
                    other6.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|18-getter|2|18-postInit
        // write post-init user code here
        return other6;//GEN-BEGIN:|18-getter|3|
    }
//GEN-END:|18-getter|3|
    public TiledLayer getOther7() throws java.io.IOException {//GEN-BEGIN:|19-getter|0|19-preInit
        if (other7 == null) {//GEN-END:|19-getter|0|19-preInit
            // write pre-init user code here
            other7 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|19-getter|1|19-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|19-getter|1|19-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|19-getter|2|19-postInit
                for (int col = 0; col < 4; col++) {
                    other7.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|19-getter|2|19-postInit
        // write post-init user code here
        return other7;//GEN-BEGIN:|19-getter|3|
    }
//GEN-END:|19-getter|3|
    public TiledLayer getOther8() throws java.io.IOException {//GEN-BEGIN:|20-getter|0|20-preInit
        if (other8 == null) {//GEN-END:|20-getter|0|20-preInit
            // write pre-init user code here
            other8 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|20-getter|1|20-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|20-getter|1|20-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|20-getter|2|20-postInit
                for (int col = 0; col < 4; col++) {
                    other8.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|20-getter|2|20-postInit
        // write post-init user code here
        return other8;//GEN-BEGIN:|20-getter|3|
    }
//GEN-END:|20-getter|3|
    public TiledLayer getOther9() throws java.io.IOException {//GEN-BEGIN:|21-getter|0|21-preInit
        if (other9 == null) {//GEN-END:|21-getter|0|21-preInit
            // write pre-init user code here
            other9 = new TiledLayer(4, 1, getCards(), 10, 27);//GEN-BEGIN:|21-getter|1|21-midInit
            int[][] tiles = {
                { 77, 78, 78, 78 }
            };//GEN-END:|21-getter|1|21-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|21-getter|2|21-postInit
                for (int col = 0; col < 4; col++) {
                    other9.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|21-getter|2|21-postInit
        // write post-init user code here
        return other9;//GEN-BEGIN:|21-getter|3|
    }
//GEN-END:|21-getter|3|
    public TiledLayer getBoxLayer() throws java.io.IOException {//GEN-BEGIN:|2137-getter|0|2137-preInit
        if (boxLayer == null) {//GEN-END:|2137-getter|0|2137-preInit
            // write pre-init user code here
            boxLayer = new TiledLayer(1, 1, getCards(), 20, 27);//GEN-BEGIN:|2137-getter|1|2137-midInit
            int[][] tiles = {
                { 39 }
            };//GEN-END:|2137-getter|1|2137-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|2137-getter|2|2137-postInit
                for (int col = 0; col < 1; col++) {
                    boxLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|2137-getter|2|2137-postInit
        // write post-init user code here
        return boxLayer;//GEN-BEGIN:|2137-getter|3|
    }
//GEN-END:|2137-getter|3|
    public TiledLayer getDown2Layer() throws java.io.IOException {//GEN-BEGIN:|2222-getter|0|2222-preInit
        if (down2Layer == null) {//GEN-END:|2222-getter|0|2222-preInit
            // write pre-init user code here
            down2Layer = new TiledLayer(3, 1, getCards(), 5, 27);//GEN-BEGIN:|2222-getter|1|2222-midInit
            int[][] tiles = {
                { 156, 156, 156 }
            };//GEN-END:|2222-getter|1|2222-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|2222-getter|2|2222-postInit
                for (int col = 0; col < 3; col++) {
                    down2Layer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|2222-getter|2|2222-postInit
        // write post-init user code here
        return down2Layer;//GEN-BEGIN:|2222-getter|3|
    }
//GEN-END:|2222-getter|3|
    public Image getSidecursors() throws java.io.IOException {//GEN-BEGIN:|3153-getter|0|3153-preInit
        if (sidecursors == null) {//GEN-END:|3153-getter|0|3153-preInit
            // write pre-init user code here
            sidecursors = Image.createImage("/data/sidecursors.png");//GEN-BEGIN:|3153-getter|1|3153-postInit
        }//GEN-END:|3153-getter|1|3153-postInit
        // write post-init user code here
        return this.sidecursors;//GEN-BEGIN:|3153-getter|2|
    }
//GEN-END:|3153-getter|2|
    public TiledLayer getCursorLeftLayer() throws java.io.IOException {//GEN-BEGIN:|3154-getter|0|3154-preInit
        if (cursorLeftLayer == null) {//GEN-END:|3154-getter|0|3154-preInit
            // write pre-init user code here
            cursorLeftLayer = new TiledLayer(1, 1, getSidecursors(), 13, 24);//GEN-BEGIN:|3154-getter|1|3154-midInit
            int[][] tiles = {
                { 1 }
            };//GEN-END:|3154-getter|1|3154-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3154-getter|2|3154-postInit
                for (int col = 0; col < 1; col++) {
                    cursorLeftLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3154-getter|2|3154-postInit
        // write post-init user code here
        return cursorLeftLayer;//GEN-BEGIN:|3154-getter|3|
    }
//GEN-END:|3154-getter|3|
    public TiledLayer getCursorRightLayer() throws java.io.IOException {//GEN-BEGIN:|3155-getter|0|3155-preInit
        if (cursorRightLayer == null) {//GEN-END:|3155-getter|0|3155-preInit
            // write pre-init user code here
            cursorRightLayer = new TiledLayer(1, 1, getSidecursors(), 13, 24);//GEN-BEGIN:|3155-getter|1|3155-midInit
            int[][] tiles = {
                { 2 }
            };//GEN-END:|3155-getter|1|3155-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3155-getter|2|3155-postInit
                for (int col = 0; col < 1; col++) {
                    cursorRightLayer.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3155-getter|2|3155-postInit
        // write post-init user code here
        return cursorRightLayer;//GEN-BEGIN:|3155-getter|3|
    }
//GEN-END:|3155-getter|3|
    public Image getNumbers() throws java.io.IOException {//GEN-BEGIN:|3629-getter|0|3629-preInit
        if (numbers == null) {//GEN-END:|3629-getter|0|3629-preInit
            // write pre-init user code here
            numbers = Image.createImage("/data/numbers.png");//GEN-BEGIN:|3629-getter|1|3629-postInit
        }//GEN-END:|3629-getter|1|3629-postInit
        // write post-init user code here
        return this.numbers;//GEN-BEGIN:|3629-getter|2|
    }
//GEN-END:|3629-getter|2|
    public TiledLayer getOther1cards() throws java.io.IOException {//GEN-BEGIN:|3630-getter|0|3630-preInit
        if (other1cards == null) {//GEN-END:|3630-getter|0|3630-preInit
            // write pre-init user code here
            other1cards = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3630-getter|1|3630-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3630-getter|1|3630-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3630-getter|2|3630-postInit
                for (int col = 0; col < 2; col++) {
                    other1cards.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3630-getter|2|3630-postInit
        // write post-init user code here
        return other1cards;//GEN-BEGIN:|3630-getter|3|
    }
//GEN-END:|3630-getter|3|
    public TiledLayer getOthers2Card() throws java.io.IOException {//GEN-BEGIN:|3721-getter|0|3721-preInit
        if (others2Card == null) {//GEN-END:|3721-getter|0|3721-preInit
            // write pre-init user code here
            others2Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3721-getter|1|3721-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3721-getter|1|3721-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3721-getter|2|3721-postInit
                for (int col = 0; col < 2; col++) {
                    others2Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3721-getter|2|3721-postInit
        // write post-init user code here
        return others2Card;//GEN-BEGIN:|3721-getter|3|
    }
//GEN-END:|3721-getter|3|
    public TiledLayer getOthers3Card() throws java.io.IOException {//GEN-BEGIN:|3722-getter|0|3722-preInit
        if (others3Card == null) {//GEN-END:|3722-getter|0|3722-preInit
            // write pre-init user code here
            others3Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3722-getter|1|3722-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3722-getter|1|3722-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3722-getter|2|3722-postInit
                for (int col = 0; col < 2; col++) {
                    others3Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3722-getter|2|3722-postInit
        // write post-init user code here
        return others3Card;//GEN-BEGIN:|3722-getter|3|
    }
//GEN-END:|3722-getter|3|
    public TiledLayer getOthers4Card() throws java.io.IOException {//GEN-BEGIN:|3723-getter|0|3723-preInit
        if (others4Card == null) {//GEN-END:|3723-getter|0|3723-preInit
            // write pre-init user code here
            others4Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3723-getter|1|3723-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3723-getter|1|3723-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3723-getter|2|3723-postInit
                for (int col = 0; col < 2; col++) {
                    others4Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3723-getter|2|3723-postInit
        // write post-init user code here
        return others4Card;//GEN-BEGIN:|3723-getter|3|
    }
//GEN-END:|3723-getter|3|
    public TiledLayer getOthers5card() throws java.io.IOException {//GEN-BEGIN:|3724-getter|0|3724-preInit
        if (others5card == null) {//GEN-END:|3724-getter|0|3724-preInit
            // write pre-init user code here
            others5card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3724-getter|1|3724-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3724-getter|1|3724-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3724-getter|2|3724-postInit
                for (int col = 0; col < 2; col++) {
                    others5card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3724-getter|2|3724-postInit
        // write post-init user code here
        return others5card;//GEN-BEGIN:|3724-getter|3|
    }
//GEN-END:|3724-getter|3|
    public TiledLayer getOthers6Card() throws java.io.IOException {//GEN-BEGIN:|3725-getter|0|3725-preInit
        if (others6Card == null) {//GEN-END:|3725-getter|0|3725-preInit
            // write pre-init user code here
            others6Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3725-getter|1|3725-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3725-getter|1|3725-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3725-getter|2|3725-postInit
                for (int col = 0; col < 2; col++) {
                    others6Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3725-getter|2|3725-postInit
        // write post-init user code here
        return others6Card;//GEN-BEGIN:|3725-getter|3|
    }
//GEN-END:|3725-getter|3|
    public TiledLayer getOthers7Card() throws java.io.IOException {//GEN-BEGIN:|3726-getter|0|3726-preInit
        if (others7Card == null) {//GEN-END:|3726-getter|0|3726-preInit
            // write pre-init user code here
            others7Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3726-getter|1|3726-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3726-getter|1|3726-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3726-getter|2|3726-postInit
                for (int col = 0; col < 2; col++) {
                    others7Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3726-getter|2|3726-postInit
        // write post-init user code here
        return others7Card;//GEN-BEGIN:|3726-getter|3|
    }
//GEN-END:|3726-getter|3|
    public TiledLayer getOthers8Card() throws java.io.IOException {//GEN-BEGIN:|3727-getter|0|3727-preInit
        if (others8Card == null) {//GEN-END:|3727-getter|0|3727-preInit
            // write pre-init user code here
            others8Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3727-getter|1|3727-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3727-getter|1|3727-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3727-getter|2|3727-postInit
                for (int col = 0; col < 2; col++) {
                    others8Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3727-getter|2|3727-postInit
        // write post-init user code here
        return others8Card;//GEN-BEGIN:|3727-getter|3|
    }
//GEN-END:|3727-getter|3|
    public TiledLayer getOthers9Card() throws java.io.IOException {//GEN-BEGIN:|3728-getter|0|3728-preInit
        if (others9Card == null) {//GEN-END:|3728-getter|0|3728-preInit
            // write pre-init user code here
            others9Card = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|3728-getter|1|3728-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|3728-getter|1|3728-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3728-getter|2|3728-postInit
                for (int col = 0; col < 2; col++) {
                    others9Card.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3728-getter|2|3728-postInit
        // write post-init user code here
        return others9Card;//GEN-BEGIN:|3728-getter|3|
    }
//GEN-END:|3728-getter|3|
    public TiledLayer getMyCarsCount() throws java.io.IOException {//GEN-BEGIN:|4298-getter|0|4298-preInit
        if (myCarsCount == null) {//GEN-END:|4298-getter|0|4298-preInit
            // write pre-init user code here
            myCarsCount = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|4298-getter|1|4298-midInit
            int[][] tiles = {
                { 0, 7 }
            };//GEN-END:|4298-getter|1|4298-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|4298-getter|2|4298-postInit
                for (int col = 0; col < 2; col++) {
                    myCarsCount.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|4298-getter|2|4298-postInit
        // write post-init user code here
        return myCarsCount;//GEN-BEGIN:|4298-getter|3|
    }
//GEN-END:|4298-getter|3|
    public TiledLayer getBoxCard() throws java.io.IOException {//GEN-BEGIN:|4434-getter|0|4434-preInit
        if (boxCard == null) {//GEN-END:|4434-getter|0|4434-preInit
            // write pre-init user code here
            boxCard = new TiledLayer(2, 1, getNumbers(), 10, 10);//GEN-BEGIN:|4434-getter|1|4434-midInit
            int[][] tiles = {
                { 10, 10 }
            };//GEN-END:|4434-getter|1|4434-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|4434-getter|2|4434-postInit
                for (int col = 0; col < 2; col++) {
                    boxCard.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|4434-getter|2|4434-postInit
        // write post-init user code here
        return boxCard;//GEN-BEGIN:|4434-getter|3|
    }
//GEN-END:|4434-getter|3|
    TiledLayer[] giveOthers(short[] others) {
        TiledLayer[] tl = new TiledLayer[9];
        tl[0] = other1;
        tl[1] = other2;
        tl[2] = other3;
        tl[3] = other4;
        tl[4] = other5;
        tl[5] = other6;
        tl[6] = other7;
        tl[7] = other8;
        tl[8] = other9;
        TiledLayer[] retTl = new TiledLayer[others.length];

        for (int i = 0; i < 9; i++) {
            tl[i].setVisible(false);
        }

        for (int i = 0; i < others.length; i++) {
            retTl[i] = tl[others[i] - 1];
            retTl[i].setVisible(true);
        }


        return retTl;


    }

    TiledLayer[] giveOthersNumbers(short[] others) {
        TiledLayer[] tlNums = new TiledLayer[9];
        tlNums[0] = other1cards;
        tlNums[1] = others2Card;
        tlNums[2] = others3Card;
        tlNums[3] = others4Card;
        tlNums[4] = others5card;
        tlNums[5] = others6Card;
        tlNums[6] = others7Card;
        tlNums[7] = others8Card;
        tlNums[8] = others9Card;

        TiledLayer[] retTl = new TiledLayer[others.length];


        for (int i = 0; i < 9; i++) {
            tlNums[i].setVisible(false);
        }

        for (int i = 0; i < others.length; i++) {
            retTl[i] = tlNums[others[i] - 1];
            retTl[i].setVisible(true);
        }

        return retTl;

    }
}

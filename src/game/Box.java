/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Random;
import java.util.Stack;
import tool.Tester;

/**
 *
 * @author e
 */
public class Box {

    public Stack allCards;
    Card[] yellows;
    Card[] red;
    Card[] green;
    Card[] blue;
    Card[] wilds;
    Card empty;
    Card closedCard;
    Card whiteCard;
    public static final short COLOR_WILD = 0;
    public static final short COLOR_YELLOW = 1;
    public static final short COLOR_RED = 2;
    public static final short COLOR_GREEN = 3;
    public static final short COLOR_BLUE = 4;
    public static final short COLOR_CLOSEDCARD = 5;
    public static final short COLOR_EMPTYCARD = 6;
    public static final short COLOR_WHITE = 7;
    public static final short NUMBER_WILD1 = 13;
    public static final short NUMBER_WILD2 = 14;
    public static final short NUMBER_EMPTYCARD = 15;
    public static final short NUMBER_CLOSEDCARD = 16;
    public static final short NUMBER_WHITECARD = 17;
    public static final short NUMBER_REVERSE = 10;
    public static final short NUMBER_CHANGECOLOR = 11;
    public static final short NUMBER_PASSNEXTPLAYER = 12;
    Random r;

    public Box() {
        allCards = new Stack();
        yellows = new Card[13];
        red = new Card[13];
        green = new Card[13];
        blue = new Card[13];
        wilds = new Card[2];
        r = new Random();
        generateCards();
        mixCard();
    }

    public void generateCards() {

        short inPicture = 0;

        empty = new Card(NUMBER_EMPTYCARD, COLOR_EMPTYCARD, inPicture++);

        for (short i = 0; i < 13; i++) {
            if (i == 10) {

                wilds[0] = new Card(NUMBER_WILD1, COLOR_WILD, (short) 11);
                inPicture++;

            }
            green[i] = new Card(i, COLOR_GREEN, (short) (inPicture + i));
        }

        inPicture = 15;
        for (short i = 0; i < 13; i++) {
            if (i == 10) {
                wilds[1] = new Card(NUMBER_WILD2, COLOR_WILD, (short) 25);
                inPicture++;

            }
            blue[i] = new Card(i, COLOR_BLUE, (short) (inPicture + i));
        }

        inPicture = 29;
        for (short i = 0; i < 13; i++) {
            if (i == 10) {
                closedCard = new Card(NUMBER_CLOSEDCARD, COLOR_CLOSEDCARD, (short) (inPicture + i));
                inPicture++;

            }
            red[i] = new Card(i, COLOR_RED, (short) (inPicture + i));
        }

        inPicture = 43;
        for (short i = 0; i < 13; i++) {
            if (i == 10) {
                whiteCard = new Card(NUMBER_WHITECARD, COLOR_WHITE, (short) (inPicture + i));
                inPicture++;

            }

            yellows[i] = new Card(i, COLOR_YELLOW, (short) (inPicture + i));
        }

    }

    private short r() {
        return (short) r.nextInt(allCards.size());
    }

    private void mixCard() {
        allCards = new Stack();

        allCards.insertElementAt(wilds[0], 0);
        allCards.insertElementAt(wilds[1], r());

        for (short i = 0; i < 13; i++) {
            allCards.insertElementAt(yellows[i], r());
            allCards.insertElementAt(red[i], r());
            allCards.insertElementAt(blue[i], r());
            allCards.insertElementAt(green[i], r());
            //Again for 108
            allCards.insertElementAt(yellows[i], r());
            allCards.insertElementAt(red[i], r());
            allCards.insertElementAt(blue[i], r());
            allCards.insertElementAt(green[i], r());
        }

        allCards.insertElementAt(wilds[0], r());
        allCards.insertElementAt(wilds[1], r());
        allCards.insertElementAt(allCards.pop(), r());
    }

    public Card giveCard() throws Exception {
        return (Card) allCards.pop();
    }

    public Card[] giveCards(short cardCount) throws Exception {
        Card[] retCards = new Card[cardCount];
        for (short i = 0; i < cardCount; i++) {
            retCards[i] = giveCard();
        }
        return retCards;

    }

    public short getCardCount() {
        return (short) allCards.size();
    }
}

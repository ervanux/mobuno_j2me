/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.EmptyStackException;
import javax.microedition.lcdui.game.TiledLayer;
import tool.Tester;

/**
 *
 * @author e
 */
public class Me extends GamePlayer {

    TiledLayer tl;

    public Me(TiledLayer tl) {
        this.tl = tl;
    }

    public void takeCard(Card card) {
        try {

            cards.push(card);
        } catch (EmptyStackException ex) {
            Tester.w(ex.getMessage());
        }
        setCardsInterface();
    }

    public Card dropCard(int cardID) {
        Card card = (Card) cards.elementAt(cardID);
        cards.removeElementAt(cardID);
        setCardsInterface();
        return card;
    }

    public void setCardsInterface() {
        int size = getCardCount();
        if (size > 7) {
            size = 7;
        }
        for (int i = 0; i < size; i++) {
            tl.setCell(i, 0, ((Card) cards.elementAt(i)).getInPicture());
        }
        if (size < 7) {
            for (int i = cards.size(); i < 7; i++) {
                tl.setCell(i, 0, 0);//Empty Card cards
            }
        }

    }

    public void setCardsInterface(int from) {

        if (cards.size() <= 7 || (cards.size() - from < 7)) {
            setCardsInterface();
        } else {
            for (int i = from; i < (from + 7); i++) {
                int a;
                if (i < cards.size()) {
                    a = ((Card) cards.elementAt(i)).getInPicture();
                } else {
                    a = 39;//Closed card position
                }
                tl.setCell(i, 0, a);
            }
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.Stack;
import tool.Tester;

/**
 *
 * @author e
 */
public class GamePlayer {

    private short playerID;
    protected String PlayerName;
    public Stack cards;

    public short getPlayerID() {
        return playerID;
    }

    short[] getAllCardIDs() throws Exception {
        short[] ret = new short[cards.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = ((Card) cards.elementAt(i)).getInPicture();
        }
        return ret;
    }

    public GamePlayer() {
        cards = new Stack();

    }

    public void takeCard(Card card) {
        cards.push(card);
    }

    public void takeCard(Card[] card) {
        for (int i = 0; i < card.length; i++) {
            takeCard(card[i]);
        }
    }

    public short getCardCount() {
        return (short) cards.size();
    }

    private Card dropCard() throws Exception {
        Card c = (Card) cards.lastElement();
        cards.removeElement(c);
        return c;
    }

    public Card dropCard(Card downCard) throws Exception {
        Card c = selectCardForDrop(downCard);
        if (c == null) {
            c = (Card) cards.pop();
        } else {
            cards.removeElement(c);
        }
        return c;
    }

    public void setPlayerID(short playerID) {
        this.playerID = playerID;
    }

    public Card getCard(short cardID) {
        if (cards.size() == 0) {
            Tester.w(playerID + ". player: Kart kalmadı.");
            return null;
        }

        return (Card) cards.elementAt(cardID);
    }

    public String toString() {
        String s = "";
        s = playerID + ":";

        for (int i = 0; i < cards.size(); i++) {
            s += "," + cards.elementAt(i);
        }

        return s;

    }

    Card selectCardForDrop(Card card) {
        for (int i = 0; i < cards.size(); i++) {
            Card t = (Card) cards.elementAt(i);
            if (t.getColor() == Box.COLOR_WILD) {
                return t;
            } else if (t.getColor() == card.getColor()) {
                if (t.getNumber() == Box.NUMBER_REVERSE ||
                        t.getNumber() == Box.NUMBER_PASSNEXTPLAYER ||
                        t.getNumber() == Box.NUMBER_CHANGECOLOR) {

                    return t;
                } else {
                    return t;
                }

            }
        }
        return (Card) cards.peek();
    }
}

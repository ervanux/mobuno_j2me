/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.io.IOException;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Graphics;
import javax.microedition.lcdui.game.*;
import start.MobunoStart;
import tool.Tester;

/**
 *
 * @author e
 */
public class MobunoGameCanvas extends GameCanvas implements Runnable {

    MobunoStart midlet;
    MobunoDesign design;
    TiledLayer myCardLayer, boxLayer, downLayer, gameCursorLayer;
    TiledLayer cursorRightLayer, cursorLeftLayer;
    TiledLayer[] others;
    TiledLayer[] othersNumbers;
    LayerManager layerManager;
    public Command exit, pause;
    public final short OTHERUSERCOUNT;
    public final short MAXVISIBLECARDCOUNT = 7;
    Graphics g;
    Box cardBox;
    GamePlayer[] gamePlayers;
    Down cardDown;
    Me me;
    GamePlayer currentPlayer;
    GamePlayer lastPlayer;
    short cursorCardIndex = 0;
    short gameSide = 1;
    short cardShownOffset = 0;
    private short cursorX, lastCursorX;
    private short cursorY, lastCursorY;
    private short meCursorCoordinateX;
    private short meCursorCoordinateY;
    private boolean interrupt = false;
    private short selectedCardIndex = 0;
    private GamePlayer nextPlayer;
    Card lastCard;
    short SPEED = 1;

    public MobunoGameCanvas(MobunoStart midlet, short userCount, short speed) {
        super(true);
        this.midlet = midlet;
        OTHERUSERCOUNT = userCount;

        initial();
        initialGame();
        initialPlayer();

        SPEED = speed;

        for (int i = 0; i < (OTHERUSERCOUNT); i++) {
            Tester.w(gamePlayers[i] + "");
        }

        lastCursorX = meCursorCoordinateX;
        lastCursorY = meCursorCoordinateY;

        //Paint for box card number
        try {
            short on = (short) (cardBox.getCardCount() / 10);
            short bir = (short) (cardBox.getCardCount() % 10);
            if (bir == 0) {
                bir = 10;
            }
            design.getBoxCard().setCell(1, 0, bir);
            design.getBoxCard().setCell(0, 0, on);
            meCursorCoordinateX = (short) design.getCursorLayer().getX();
            meCursorCoordinateY = (short) design.getCursorLayer().getY();

        } catch (IOException ex) {
            ex.printStackTrace();
        }



        layerManager.paint(g, 0, 0);
        flushGraphics();

    }

    private void initial() {
        try {
            g = getGraphics();
            setFullScreenMode(true);
            exit = new Command("exit", Command.EXIT, 0);
            pause = new Command("pause", Command.BACK, 1);
            addCommand(exit);
            addCommand(pause);
            setCommandListener(midlet);
            design = new MobunoDesign();
            myCardLayer = design.getMyCardLayer();
            boxLayer = design.getBoxLayer();
            downLayer = design.getDown1Layer();
            gameCursorLayer = design.getCursorLayer();
            cursorLeftLayer = design.getCursorLeftLayer();
            cursorRightLayer = design.getCursorRightLayer();
            layerManager = new LayerManager();
            design.updateLayerManagerForScene(layerManager);
        } catch (IOException ex) {
            Tester.w(ex.getMessage());
        }
    }

    public void initialGame() {
        cardBox = new Box();
        cardDown = new Down(downLayer);
        gamePlayers = new GamePlayer[OTHERUSERCOUNT];
        me = new Me(myCardLayer);
        for (short i = 0; i < (OTHERUSERCOUNT); i++) {
            gamePlayers[i] = new GamePlayer();
            try {
                gamePlayers[i].takeCard(cardBox.giveCards((short) 7));
                //Tester.w(gamePlayers[i] + "");
            } catch (Exception ex) {
                boxEmpty();
            }
            //Tester.w(gamePlayers[i] + "");
        }
        try {
            me.takeCard(cardBox.giveCards((short) 7));
            cardDown.takefirstCard(cardBox.giveCard());
        } catch (Exception ex) {
            boxEmpty();
        }
        lastCard = cardDown.getTopCard();
        currentPlayer = me;


    }

    private void initialPlayer() {
        short[] o = null;
        switch (OTHERUSERCOUNT) {
            case 1:
                o = new short[]{5};
                break;
            case 2:
                o = new short[]{3, 7};
                break;
            case 3:
                o = new short[]{2, 5, 8};
                break;
            case 4:
                o = new short[]{2, 4, 6, 8};
                break;
            case 5:
                o = new short[]{1, 3, 5, 7, 9};
                break;
            case 6:
                o = new short[]{1, 3, 4, 6, 7, 9};
                break;
            case 7:
                o = new short[]{1, 2, 3, 5, 7, 8, 9};
                break;
            case 8:
                o = new short[]{1, 2, 3, 4, 6, 7, 8, 9};
                break;
            case 9:
                o = new short[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
                break;
        }

        me.setPlayerID((short) 0);
        for (int i = 0; i < OTHERUSERCOUNT; i++) {
            gamePlayers[i].setPlayerID(o[i]);
        }
        others = design.giveOthers(o);
        othersNumbers = design.giveOthersNumbers(o);
    }

    void setOthersNumbers() {
        try {
//me
            short bir = (short) (me.getCardCount() % 10);
            if (bir == 0) {
                bir = 10;
            }
            design.getMyCarsCount().setCell(1, 0, bir); //1ler basamağı
            design.getMyCarsCount().setCell(0, 0, me.getCardCount() / 10); //10lar basamağı
//Other
            for (int i = 0; i < gamePlayers.length; i++) {
                if (currentPlayer == gamePlayers[i]) {
                    bir = (short) (currentPlayer.getCardCount() % 10);
                    if (bir == 0) {
                        bir = 10;
                    }
                    othersNumbers[i].setCell(1, 0, bir); //birler basamağı
                    othersNumbers[i].setCell(0, 0, currentPlayer.getCardCount() / 10); //10lar basamağı
                    //Tester.w(" Current " + currentPlayer.getCardCount());
                    break;
                }
            }
//Box
            short on = (short) (cardBox.getCardCount() / 10);
            bir = (short) (cardBox.getCardCount() % 10);
            if (bir == 0) {
                bir = 10;
            }
            design.getBoxCard().setCell(1, 0, bir); //birler basamağı
            design.getBoxCard().setCell(0, 0, on);
        } catch (IOException ex) {
            Tester.w(ex.getMessage());
        }

    }

    void setCoordinatePlayerID() {
        switch (currentPlayer.getPlayerID()) {
            case 0: {
                if (cursorCardIndex > me.getCardCount()) {
                    meCursorCoordinateX -= 20;
                    Tester.w("Burada");
                }

                cursorX = meCursorCoordinateX;
                cursorY = meCursorCoordinateY;

                break;
            }

            case 1: {
                cursorX = 50;
                cursorY = 100;
                break;
            }
            case 2: {
                cursorX = 50;
                cursorY = 70;
                break;
            }
            case 3: {
                cursorX = 50;
                cursorY = 40;
                break;
            }
            case 4: {
                cursorX = 50;
                cursorY = 10;
                break;
            }

            case 5: {
                cursorX = 110;
                cursorY = 10;
                break;
            }
            case 6: {
                cursorX = 165;
                cursorY = 10;
                break;

            }
            case 7: {
                cursorX = 120;
                cursorY = 40;
                break;
            }
            case 8: {
                cursorX = 120;
                cursorY = 70;
                break;
            }
            case 9: {
                cursorX = 120;
                cursorY = 100;
                break;
            }
        }
    }

    public void interrupt() {
        interrupt = true;
    }

    public void run() {
        int keys;

        while (!interrupt) {
            if (currentPlayer == me) {
                keys = getKeyStates();
                if ((keys & RIGHT_PRESSED) != 0) {
                    if ((cursorCardIndex < me.getCardCount() - 1) &&
                            (cursorCardIndex < 6)) {
                        meCursorCoordinateX += 20;
                        meCursorCoordinateY = (short) gameCursorLayer.getY();
                        cursorCardIndex++;
                        selectedCardIndex++;
                    } else if ((me.getCardCount() > MAXVISIBLECARDCOUNT) &&
                            (cursorCardIndex == MAXVISIBLECARDCOUNT - 1) &&
                            (selectedCardIndex != me.getCardCount() - 1)) {
                        myCardLayerRefresh(RIGHT);
                        selectedCardIndex++;
                    }

                    if (selectedCardIndex + 1 == me.getCardCount()) {
                        cursorRightLayer.setVisible(false);
                    }

                } else if ((keys & LEFT_PRESSED) != 0) {
                    if ((cursorCardIndex != 0)) {
                        meCursorCoordinateX -= 20;
                        meCursorCoordinateY = (short) gameCursorLayer.getY();
                        cursorCardIndex--;
                        selectedCardIndex--;
                    } else if ((me.getCardCount() > MAXVISIBLECARDCOUNT) &&
                            (cursorCardIndex == 0) &&
                            (selectedCardIndex != 0)) {

                        myCardLayerRefresh(LEFT);
                        selectedCardIndex--;
                    }
                    if (selectedCardIndex == 0) {
                        cursorLeftLayer.setVisible(false);
                    }
                } else if ((keys & DOWN_PRESSED) != 0) {
                    try {
                        me.takeCard(cardBox.giveCard());
                    } catch (Exception e) {
                        boxEmpty();
                    }
                    setOthersNumbers();
                    currentPlayer = nextPlayer();
                    if (me.getCardCount() > MAXVISIBLECARDCOUNT) {
                        cursorRightLayer.setVisible(true);
                    }
                } else if ((keys & FIRE_PRESSED) != 0) {
                    Card c = null;
                    try {
                        c = me.getCard(selectedCardIndex);
                    } catch (Exception e) {
                        midlet.Win(gamePlayers[0].getCardCount());
                        interrupt = true;
                    }
                    if (cardDown.canBe(c)) {
                        applyNewCardEffect(c);
                        try {
                            cardDown.takeCard(me.dropCard(selectedCardIndex));
                        } catch (Exception e) {
                            midlet.Win(gamePlayers[0].getCardCount());
                            interrupt = true;
                        }
                        if (selectedCardIndex == me.getCardCount()) {
                            selectedCardIndex--;
                            if (MAXVISIBLECARDCOUNT > me.getCardCount()) {
                                cursorCardIndex--;
                            }
                            meCursorCoordinateX -= 20;
                        }
                        setOthersNumbers();
                        lastCard = cardDown.getTopCard();
                        if (me.getCardCount() == 0) {
                            midlet.Win(gamePlayers[0].getCardCount());
                            interrupt = true;
                        }
                        if (nextPlayer == null) {
                            currentPlayer = nextPlayer();
                        } else {
                            currentPlayer = nextPlayer;
                            nextPlayer = null;
                        }
                    }

                }
            } else {//current user not me
                // Tester.w("down:" + cardDown.getTopCard() + " " + currentPlayer.getPlayerID() + ".player: " + currentPlayer.selectCardForDrop(lastCard));
                if (cardDown.canBe(currentPlayer.selectCardForDrop(lastCard))) {
                    applyNewCardEffect(currentPlayer.selectCardForDrop(lastCard));
                    try {
                        cardDown.takeCard(currentPlayer.dropCard(lastCard));
                    } catch (Exception e) {
                        midlet.gameOver(currentPlayer.getPlayerID());
                        interrupt = true;
                    }
                    lastCard = cardDown.getTopCard();
                    if (currentPlayer.getCardCount() == 0) {
                        midlet.gameOver(currentPlayer.getPlayerID());
                        interrupt = true;
                    }
                } else {
                    try {
                        currentPlayer.takeCard(cardBox.giveCard());
                    } catch (Exception ex) {
                        boxEmpty();
                    }
                }
                setOthersNumbers();
                currentPlayer = nextPlayer();
            }

            setCoordinatePlayerID();
            if (cursorX != lastCursorX || cursorY != lastCursorY) {
                gameCursorLayer.setPosition(cursorX, cursorY);
                lastCursorX = cursorX;
                lastCursorY = cursorY;
            }

            layerManager.paint(g, 0, 0);
            flushGraphics();

            try {
                Thread.sleep(SPEED * 100);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

        }
    }

    private void applyNewCardEffect(Card card) {
        try {
            switch (card.getNumber()) {
                case Box.NUMBER_WILD1:
                    nextPlayer().takeCard(cardBox.giveCard());
                    break;
                case Box.NUMBER_WILD2:
                    nextPlayer().takeCard(cardBox.giveCards((short) 4));
                    break;
                case Box.NUMBER_REVERSE:
                    gameSide *= -1;
                    break;
                case Box.NUMBER_CHANGECOLOR:
                    break;
                case Box.NUMBER_PASSNEXTPLAYER:
                    nextPlayer = nextPlayer();
                    nextPlayer = nextPlayer();//Pass next user
                    break;
            }
        } catch (Exception ex) {
            boxEmpty();
        }


    }

    private GamePlayer nextPlayer() {

        if (currentPlayer == me) {
            if (gameSide == -1) {
                return gamePlayers[OTHERUSERCOUNT - 1];
            } else {
                return gamePlayers[0];
            }

        } else {
            for (int i = 0; i <
                    OTHERUSERCOUNT; i++) {
                if (currentPlayer == gamePlayers[i]) {
                    i += gameSide;
                    if (i == -1 || i == OTHERUSERCOUNT) {
                        return me;
                    } else {
                        return gamePlayers[i];
                    }

                }
            }
        }
        return null;

    }

    private void boxEmpty() {
        boxLayer.setVisible(false);
        short id = 0;
        short cardCount = me.getCardCount();
        for (int i = 0; i <
                OTHERUSERCOUNT; i++) {
            if (cardCount > gamePlayers[i].getCardCount()) {
                cardCount = gamePlayers[i].getCardCount();
                id = gamePlayers[i].getPlayerID();
            }
        }
        if (id == 0) {
            midlet.Win(gamePlayers[0].getCardCount());
        } else {
            midlet.gameOver(id);
        }
        interrupt = true;
    }

    private void myCardLayerRefresh(int side) {

        try {
            short[] ids = me.getAllCardIDs();
            switch (side) {
                case RIGHT:
                    cardShownOffset++;
                    cursorLeftLayer.setVisible(true);
                    break;
                case LEFT:
                    cardShownOffset--;
                    cursorRightLayer.setVisible(true);
                    break;
            }
            short j = 0;
            for (int i = cardShownOffset; i < cardShownOffset + 7; i++) {
                myCardLayer.setCell(j++, 0, ids[i]);
            }

        } catch (Exception e) {
            Tester.w(e.getMessage());
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import game.MobunoGameCanvas;
import java.util.Random;
import javax.microedition.lcdui.*;
import javax.microedition.media.MediaException;
import javax.microedition.midlet.*;
import menu.MenuCanvas;
import tool.DataManager;
import tool.MusicPlayer;

/**
 * @author e
 */
public class MobunoStart extends MIDlet implements CommandListener, ItemStateListener {

    SettingForm settingManager;
    public MusicPlayer player;
    Display display;
    MenuCanvas menuCanvas;
    public MobunoGameCanvas mobunoGameCanvas;
    DataManager dataManager;
    HighScoreForm highScoreForm;
    Thread menuThread;
    public Thread gameThread;
    Random r;
    boolean gameOver = true;
    public Form helpForm;
    Command c;

    public MobunoStart() {
        initial();
    }

    private void initial() {
        display = Display.getDisplay(this);
        dataManager = new DataManager();
        settingManager = new SettingForm(this);
        player = new MusicPlayer(settingManager.getVolumeLevel());
        highScoreForm = new HighScoreForm(this);
        menuCanvas = new MenuCanvas(this);
        r = new Random();
        helpForm = new Form("Help");
        helpForm.append("*UNO GAME");
        helpForm.append("-Toplam 108 kart bulunmaktadır.");
        helpForm.append("-Her kullanıcıya oyun başında 7 kart verilmektedir.");
        helpForm.append("-Kart almak için aşağıya basın.");
        helpForm.append("-Kartı atmak için sağ sol tuşları ile üzerine gelin ve OK a basın");
        c = new Command("back", Command.BACK, 0);
        helpForm.addCommand(c);
        helpForm.setCommandListener(this);


    }

    public void startApp() {
        showMenu();
        menuThread = new Thread(menuCanvas);
        menuThread.start();
    }

    public void showSetting() {
        display.setCurrent(settingManager);
        stopMenu();
    }

    public void showHelp() {
        display.setCurrent(helpForm);
        stopMenu();
    }

    public void showHighScore() {
        highScoreForm.refreshScores();
        display.setCurrent(highScoreForm);
        stopMenu();

    }

    /**
     * Continue menu thread after other form
     */
    public void showMenu() {

        if (menuThread != null) {
            menuThread.interrupt();
        }
        display.setCurrent(menuCanvas);
        player.playForMenu();
    }

    /**
     * Stop menu thread when other forms is showing.
     */
    public void stopMenu() {
        if (menuThread != null) {
            try {
                player.stopPlayer();
                menuThread.join();
            } catch (MediaException ex) {
                ex.printStackTrace();
            } catch (InterruptedException ex) {
            }
        }

    }

    /**
     * Start play Game
     */
    public void startNewGame() {
        if (gameThread != null) {
            try {
                mobunoGameCanvas.interrupt();
                gameThread.join();
                gameThread = null;
                mobunoGameCanvas = null;
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        mobunoGameCanvas = new MobunoGameCanvas(this, settingManager.getUserCount(), settingManager.getSpeed());
        gameThread = new Thread(mobunoGameCanvas);
        gameThread.start();
        display.setCurrent(mobunoGameCanvas);
        stopMenu();


    }

    public void continueGame() {
        gameThread = new Thread(mobunoGameCanvas);
        gameThread.start();
        display.setCurrent(mobunoGameCanvas);
        stopMenu();
    }

    public void pauseApp() {
    }

    public void destroyApp(boolean unconditional) {
    }

    public void commandAction(Command c, Displayable d) {

        if (d == settingManager) {//commands for Setting
            if (c == settingManager.back) {
                showMenu();
            } else if (c == settingManager.save) {
                dataManager.saveSetting(settingManager.getSelectedID(), settingManager.getVolumeLevel());
                Alert a = new Alert("Save setting", "your setting saving...", null, AlertType.CONFIRMATION);
                a.setTimeout(3000);
                display.setCurrent(a);
            }
        } else if (d == highScoreForm) {//  commands for high score
            if (c == highScoreForm.back) {
                showMenu();
            }
        } else if (d == mobunoGameCanvas) {
            if (c == mobunoGameCanvas.exit) {
                try {
                    mobunoGameCanvas.interrupt();
                    gameThread.join();
                    gameThread = null;
                    mobunoGameCanvas = null;
                    showMenu();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            } else if (c == mobunoGameCanvas.pause) {
                showMenu();
            }

        } else if (d == helpForm) {
            if (c == this.c) {
                showMenu();
            }
        }
    }

    public void exitMIDlet() {
        dataManager.exit();
        destroyApp(true);
        notifyDestroyed();
    }

    public void itemStateChanged(Item item) {
        if (item == settingManager.gauge) {
            player.setVolumeLevel(settingManager.getVolumeLevel());
        }
    }

    public void gameOver(int playerID) {
        Alert a = new Alert("Game over", playerID + ". player wins", null, AlertType.INFO);
        a.setTimeout(Alert.FOREVER);
        display.setCurrent(a);

    }

    public void Win(int i) {
        dataManager.addNewScore(i * 100);
        Alert a = new Alert("You win", "CONGRULATION!!\n You win", null, AlertType.CONFIRMATION);
        a.setTimeout(Alert.FOREVER);
        display.setCurrent(a);
        mobunoGameCanvas.interrupt();
    }
}




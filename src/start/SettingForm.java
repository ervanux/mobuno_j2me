/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import javax.microedition.lcdui.*;

/**
 *
 * @author e
 */
public class SettingForm extends Form {

    protected Command back;
    protected Command save;
    private MobunoStart midlet;
    protected Gauge gauge;
    private ChoiceGroup choice;
    private ChoiceGroup choiceSpeed;
    private String[] choiceString = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};

    public SettingForm(MobunoStart midlet) {
        super("Settings");
        this.midlet = midlet;
        initial();
    }

    private void initial() {
        back = new Command("back", Command.EXIT, 0);
        save = new Command("Save", Command.OK, 0);
        addCommand(back);
        addCommand(save);
        setCommandListener(midlet);
        setItemStateListener(midlet);
        choice = new ChoiceGroup("Player count", Choice.POPUP, choiceString, null);
        choiceSpeed = new ChoiceGroup("Game Speed", Choice.POPUP, choiceString, null);
        gauge = new Gauge("Voice", true, 10, 0);

        //get saved setting
        int[] set = midlet.dataManager.getSetting();
        if (set != null) {
            choice.setSelectedIndex(set[0], true);
            choiceSpeed.setSelectedIndex(3, true);
            gauge.setValue(set[1]);
        }
        append(choice);
        append(choiceSpeed);
        append(gauge);
    }

    public int getVolumeLevel() {
        return gauge.getValue();
    }

    public short getUserCount() {//for User name
        return (short) Integer.parseInt(choice.getString(choice.getSelectedIndex()));
    }

    public int getSelectedID() {//for selected ID for datamanager
        return choice.getSelectedIndex();
    }

    public short getSpeed() {
        return (short) Integer.parseInt(choiceSpeed.getString(choiceSpeed.getSelectedIndex()));
    }
}

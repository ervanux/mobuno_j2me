/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package start;

import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Form;

/**
 *
 * @author e
 */
public class HighScoreForm extends Form {

    Command back;
    MobunoStart midlet;

    public HighScoreForm(MobunoStart midlet) {
        super("High Scores");
        this.midlet = midlet;
        initial();
        refreshScores();

    }

    private void initial() {
        back = new Command("back", Command.BACK, 0);
        addCommand(back);
        setCommandListener(midlet);

    }

    public void refreshScores() {
        deleteAll();
        int[] scores = midlet.dataManager.getScores();
        if (scores != null) {
            for (int i = 0; i < scores.length; i++) {
                append((i + 1) + " . " + scores[i] + "\n");
            }
        }
        if (size() == 0) {
            append("There isn't score");
        }
    }
}

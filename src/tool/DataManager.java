/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tool;

import java.io.*;
import javax.microedition.rms.*;

/**
 *
 * @author e
 */
public class DataManager {

    RecordStore forSetting;
    RecordStore highScore;
    String SETTING_DATA = "setdata";
    String SCORE_DATA = "scoredata";
    private final int MAXSCORE = 10;

    /**
     * Open record stores
     */
    public DataManager() {
        try {
            forSetting = RecordStore.openRecordStore(SETTING_DATA, true);
        } catch (Exception ex) {
            MobEx.ex("DataManager", "DataManager", ex);
        }

        try {//first created score record,enter 0 * 10.
            highScore = RecordStore.openRecordStore(SCORE_DATA, true);
        } catch (Exception ex) {
        }
        try {
            if (highScore.getNumRecords() == 0) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                DataOutputStream dos = new DataOutputStream(baos);
                for (int i = 0; i < MAXSCORE; i++) {
                    dos.writeInt(0);
                    highScore.addRecord(baos.toByteArray(), 0, baos.toByteArray().length);
                    baos.reset();
                }
            }
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    /**
     * Get setting from record store
     * @return setting's array
     */
    public int[] getSetting() {
        int[] data = new int[2];
        try {
            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(forSetting.getRecord(1)));
            data[0] = dis.readInt();
            data[1] = dis.readInt();
        } catch (Exception ex) {
            return null;
        }
        return data;
    }

    /**
     *
     * @param voice
     * @param userCount
     *
     * save setting
     */
    public void saveSetting(int voice, int userCount) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            dos.writeInt(voice);
            dos.writeInt(userCount);
            byte[] byt = baos.toByteArray();
            if (forSetting.getNumRecords() < 1) {
                forSetting.addRecord(byt, 0, byt.length);
            } else {
                forSetting.setRecord(1, byt, 0, byt.length);
            }
        } catch (Exception ex) {
            MobEx.ex("DataManager", "SaveSetting", ex);
        } finally {
            try {
                baos.flush();
                baos.close();
                dos.close();
            } catch (IOException ex) {
                MobEx.ex("DataManager", "SaveSetting_finnaly", ex);
            }
        }
    }

    /**
     * For add new score record
     * Add new score.if score added, return order number,else -1.
     * @return score's order number
     */
    public int addNewScore(int newScore) {
        int scores[] = getScores();
        try {
            for (int i = 0; i < MAXSCORE; i++) {
                if (newScore > scores[i]) {
                    for (int j = MAXSCORE - 2; j >= i; j--) {
                        scores[j + 1] = scores[j];
                    }
                    scores[i] = newScore;
                    setAllScores(scores);
                    return i + 1;
                }
            }
        } catch (Exception ex) {
            MobEx.ex("DataManager", "addNewScore", ex);
        }
        return -1;
    }

    /**
     * write aganin all reordered record of score
     */
    private void setAllScores(int[] scores) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            for (int i = 0; i < MAXSCORE; i++) {
                dos.writeInt(scores[i]);
                highScore.setRecord(i + 1, baos.toByteArray(), 0, baos.toByteArray().length);
                baos.reset();
            }
        } catch (Exception ex) {
            MobEx.ex("DataManager", "setAllScore", ex);
        } finally {
            try {
                baos.flush();
                baos.close();
                dos.close();
            } catch (IOException ex) {
                MobEx.ex("DataManager", "setAllScore_finally", ex);
            }
        }
    }

    /**
     * get scores from recstore
     * @return score array
     */
    public int[] getScores() {
        int[] data = null;
        try {
            int scoreCount = highScore.getNumRecords();
            if (scoreCount < 1) {
                return null;
            }
            data = new int[scoreCount];
            RecordEnumeration re = highScore.enumerateRecords(null, null, true);
            for (int i = 0; re.hasNextElement(); i++) {
                data[i] = new DataInputStream(new ByteArrayInputStream(re.nextRecord())).readInt();
            }

        } catch (Exception ex) {
            MobEx.ex("DataManager", "getScores", ex);
        }
        return data;
    }

    /**
     * Closed all records
     */
    public void exit() {
        try {
            forSetting.closeRecordStore();
            highScore.closeRecordStore();
        } catch (Exception ex) {
            MobEx.ex("DataManager", "exit", ex);
        }
    }
}

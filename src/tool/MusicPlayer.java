/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package tool;

import java.io.IOException;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VolumeControl;

/**
 *
 * @author e
 */
public class MusicPlayer {

    private Player player;
    private VolumeControl volumeControl;
    private int volumeLevel;
    private final String MENUMUSIC = "/data/menu.wav";
    private final String GAMEMUSIC = "/data/game.wav";
    private final String MENUTONE = "/data/blip.wav";

    public MusicPlayer(int volumeLevel) {
        this.volumeLevel = volumeLevel * 10;
    }

    public void playForMenu() {
        initialPlayer(MENUMUSIC);

    }

    public void playForGame() {
        initialPlayer(GAMEMUSIC);
    }

    public void playMenuTone() {
        initialPlayer(MENUTONE);
    }

    private void initialPlayer(String mediaAddress) {
        try {
            stopPlayer();
            player = Manager.createPlayer(getClass().getResourceAsStream(mediaAddress), "audio/x-wav");
            player.realize();
            volumeControl = (VolumeControl) player.getControl("VolumeControl");
            if (volumeControl != null) {
                volumeControl.setLevel(volumeLevel);
            }
            player.prefetch();
            if (player != null && player.getState() == Player.PREFETCHED) {
                player.start();
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (MediaException ex) {
            ex.printStackTrace();
        }
    }

    public void stopPlayer() throws MediaException {
        if (player != null) {
            if (player.getState() == Player.STARTED) {
                player.stop();
            }
            if (player.getState() == Player.PREFETCHED) {
                player.deallocate();
            }
            if (player.getState() == Player.REALIZED ||
                    player.getState() == Player.UNREALIZED) {
                player.close();
            }
        }
        player = null;
    }

    public void setVolumeLevel(int volumeLevel) {
        this.volumeLevel = volumeLevel * 10;
        volumeControl.setLevel(volumeLevel);
    }
}

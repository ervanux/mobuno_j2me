/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;

/**
 * @author e
 */
public class Menu {

    //<editor-fold defaultstate="collapsed" desc=" Generated Fields ">//GEN-BEGIN:|fields|0|
    private Image main001;
    private TiledLayer mainBack;
    private Image cursor2;
    private Sprite cursor2Sprite;
    public int cursor2Spriteseq003Delay = 100;
    public int[] cursor2Spriteseq003 = {0, 3, 3, 3};
    //</editor-fold>//GEN-END:|fields|0|

    //<editor-fold defaultstate="collapsed" desc=" Generated Methods ">//GEN-BEGIN:|methods|0|
    //</editor-fold>//GEN-END:|methods|0|
    public void updateLayerManagerForMain(LayerManager lm) throws java.io.IOException {//GEN-LINE:|1-updateLayerManager|0|1-preUpdate
        // write pre-update user code here
        getCursor2Sprite().setPosition(5, 30);//GEN-BEGIN:|1-updateLayerManager|1|1-postUpdate
        getCursor2Sprite().setVisible(true);
        lm.append(getCursor2Sprite());
        getMainBack().setPosition(0, 0);
        getMainBack().setVisible(true);
        lm.append(getMainBack());//GEN-END:|1-updateLayerManager|1|1-postUpdate
        // write post-update user code here
    }//GEN-BEGIN:|1-updateLayerManager|2|
//GEN-END:|1-updateLayerManager|2|
    public Image getMain001() throws java.io.IOException {//GEN-BEGIN:|2-getter|0|2-preInit
        if (main001 == null) {//GEN-END:|2-getter|0|2-preInit
            // write pre-init user code here
            main001 = Image.createImage("/data/main.png");//GEN-BEGIN:|2-getter|1|2-postInit
        }//GEN-END:|2-getter|1|2-postInit
        // write post-init user code here
        return this.main001;//GEN-BEGIN:|2-getter|2|
    }
//GEN-END:|2-getter|2|
    public TiledLayer getMainBack() throws java.io.IOException {//GEN-BEGIN:|3-getter|0|3-preInit
        if (mainBack == null) {//GEN-END:|3-getter|0|3-preInit
            // write pre-init user code here
            mainBack = new TiledLayer(1, 1, getMain001(), 176, 208);//GEN-BEGIN:|3-getter|1|3-midInit
            int[][] tiles = {
                { 1 }
            };//GEN-END:|3-getter|1|3-midInit
            // write mid-init user code here
            for (int row = 0; row < 1; row++) {//GEN-BEGIN:|3-getter|2|3-postInit
                for (int col = 0; col < 1; col++) {
                    mainBack.setCell(col, row, tiles[row][col]);
                }
            }
        }//GEN-END:|3-getter|2|3-postInit
        // write post-init user code here
        return mainBack;//GEN-BEGIN:|3-getter|3|
    }
//GEN-END:|3-getter|3|
    public Image getCursor2() throws java.io.IOException {//GEN-BEGIN:|20-getter|0|20-preInit
        if (cursor2 == null) {//GEN-END:|20-getter|0|20-preInit
            // write pre-init user code here
            cursor2 = Image.createImage("/data/cursor2.png");//GEN-BEGIN:|20-getter|1|20-postInit
        }//GEN-END:|20-getter|1|20-postInit
        // write post-init user code here
        return this.cursor2;//GEN-BEGIN:|20-getter|2|
    }
//GEN-END:|20-getter|2|
    public Sprite getCursor2Sprite() throws java.io.IOException {//GEN-BEGIN:|43-getter|0|43-preInit
        if (cursor2Sprite == null) {//GEN-END:|43-getter|0|43-preInit
            // write pre-init user code here
            cursor2Sprite = new Sprite(getCursor2(), 25, 20);//GEN-BEGIN:|43-getter|1|43-postInit
            cursor2Sprite.setFrameSequence(cursor2Spriteseq003);//GEN-END:|43-getter|1|43-postInit
            // write post-init user code here
        }//GEN-BEGIN:|43-getter|2|
        return cursor2Sprite;
    }
//GEN-END:|43-getter|2|
}

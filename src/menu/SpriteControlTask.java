/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.TimerTask;
import javax.microedition.lcdui.game.Sprite;

/**
 *
 * @author e
 */
public class SpriteControlTask extends TimerTask {

    Sprite sp;
    int seqSize;
    boolean cont;

    public SpriteControlTask(Sprite sp, boolean cont) {
        this.sp = sp;
        seqSize = sp.getFrameSequenceLength();
        this.cont = cont;
    }

    public void run() {
        sp.nextFrame();
    }
}



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import java.util.Timer;
import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;
import javax.microedition.media.*;

import start.*;

/**
 * @author e
 */
public class MenuCanvas extends GameCanvas implements Runnable {

    /**
     * constructor
     */
    LayerManager lm;
    TiledLayer tl;
    Sprite cur;
    Command exit;
    Menu mainMenu;
    MobunoStart midlet;
    int[] selections = {30, 53, 80, 105, 125};
    int selectionIndex = 0;
    private boolean interrupt = false;
    private Player player;

    public MenuCanvas(MobunoStart midlet) {
        super(true);
        this.midlet = midlet;
        try {
            this.setFullScreenMode(true);
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void init() throws Exception {
        this.mainMenu = new Menu();//Main menıu test
        this.cur = mainMenu.getCursor2Sprite();
        SpriteControlTask sct = new SpriteControlTask(cur, true);
        Timer t = new Timer();
        //paintCards();
        paintMainMenu();//Prepare main menu
        t.scheduleAtFixedRate(sct, 0, 200);
        //Music
        player = Manager.createPlayer(getClass().getResourceAsStream("/data/blip.wav"), "audio/x-wav");
    }

    public void setInterrupt(boolean interrupt) {
        this.interrupt = interrupt;
    }

    private void playFromResource() {
        try {
            player.start();
        } catch (MediaException me) {
            System.out.println("Media:" + me.getMessage());
        }

    }

    private void paintMainMenu() {
        Graphics g = getGraphics();
        try {
            lm = new LayerManager();
            mainMenu.updateLayerManagerForMain(lm);
            lm.paint(g, 0, 0);
            flushGraphics();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        Graphics g = getGraphics();
        while (true) {
            int keys = getKeyStates();
            //Pressed keys controls
            if ((keys & DOWN_PRESSED) != 0) {
                if (selectionIndex != (selections.length - 1)) {
                    selectionIndex++;
                }

            } else if ((keys & UP_PRESSED) != 0) {
                if (selectionIndex != 0) {
                    selectionIndex--;
                }

            } else if ((keys & FIRE_PRESSED) != 0) {
                switch (selectionIndex) {
                    case 0:
                        //play game
                        if (midlet.gameThread == null) {
                            midlet.startNewGame();
                        } else {
                            midlet.continueGame();
                        }
                        break;
                    case 1:
                        midlet.showHighScore();
                        break;
                    case 2:
                        midlet.showSetting();
                        break;
                    case 3:
                        midlet.showHelp();
                        break;
                    case 4:
                        midlet.exitMIDlet();
                        break;
                    case 5:
                    default:
                        System.out.println("Wrong selection!!");
                }
            }

            cur.setPosition(cur.getX(), selections[selectionIndex]);
            this.lm.paint(g, 0, 0);
            flushGraphics(0, 0, this.getWidth(), this.getHeight());
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
    }
}
